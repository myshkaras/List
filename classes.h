#include <iostream>
#define DEBUG

#ifdef DEBUG

#endif

struct listNode
{
    private:
        int value;
        listNode* previousNode;
        listNode* nextNode;

    public:
        listNode(int _value,listNode* _previousNode, listNode* _nextNode)
        {
            value = _value;
            previousNode = _previousNode;
            nextNode = _nextNode;
        }

        int getValue ()
        {
            return value;
        }

        listNode* getPrevNode ()
        {
            return previousNode;
        }

        listNode* getNextNode ()
        {
            return nextNode;
        }

        void setValue (int _value)
        {
            value = _value;
        }

        void setPrevNode (listNode* _prevNodePtr)
        {
            previousNode = _prevNodePtr;
        }

        void setNextNode (listNode* _nextNodeptr)
        {
            nextNode = _nextNodeptr;
        }

        ~listNode (){}

        #ifdef DEBUG
        void viewNodeContents ()
        {
            std::cout << "-IN_VIEW_NODE_CONTENTS-\n";
            std::cout << "Prev Node Ptr: " << previousNode << std::endl;
            std::cout << "Next Node Ptr: " << nextNode << std::endl;
            std::cout << "Node value: " << value << std::endl;
        }
        #endif
};

class ListIterator
{
    private:
        int nodeNum;
        listNode* currentNode;

    public:
        int getNodeNum ()
        {
            return nodeNum;
        }

        listNode* getCurrentNode()
        {
            return currentNode;
        }

        ListIterator(listNode* Head)
        {
            nodeNum = 1;
            currentNode = Head;
        }

        ListIterator(listNode* Tail, int amountOfNodes)
        {
            nodeNum = amountOfNodes;
            currentNode = Tail;
        }

        void moveForward ()
        {
            if (currentNode->getNextNode() != nullptr)
            {
                currentNode = currentNode->getNextNode();
                ++nodeNum;
            }
            #ifdef DEBUG
            else 
            {
                std::cout << "-IN_ITER_MOVE_FORWARD-\n There is no next node!!!\n";
            }
            #endif
        }

        void moveBackwards ()
        {
            if (currentNode->getPrevNode() != nullptr)
            {
                currentNode = currentNode->getPrevNode();
                --nodeNum;
            }
            #ifdef DEBUG
            else 
            {
                std::cout << "-IN_ITER_MOVE_BACKWARDS-\nThere is no prevoius Node !!!\n";
            }
            #endif
        }

        #ifdef DEBUG
        void viewContents ()
        {
            std::cout << "-IN_VIEW_ITER_CONTENTS\n";
            std::cout << "NodeNum = " << nodeNum << std::endl;
            std::cout << "Current Node = " << currentNode << std::endl;
        }
        #endif
};

class ListInterface
{
    private:
        unsigned int amountOfNodes;
        listNode* Head;
        listNode* Tail;

    public:
        ListInterface()
        {
            amountOfNodes = 0;
            Head = nullptr;
            Tail = nullptr;
        }

        int getAmountOfNodes ()
        {
            return amountOfNodes;
        }

        listNode* getHead ()
        {
            return Head;
        }

        listNode* getTail ()
        {
            return Tail;
        }

        void push_back (int newNodeValue)
        {
            if (amountOfNodes == 0)
            {
                listNode *newNode = new listNode(newNodeValue, nullptr, nullptr);
                Head = newNode;
                Tail = newNode;
                ++amountOfNodes;
                
                return;
            }
            
            if (amountOfNodes > 0)
            {
                listNode *newNode = new listNode(newNodeValue, Tail, nullptr);
                Tail->setNextNode(newNode);
                Tail = newNode;
                ++amountOfNodes;
                
                return;
            }

        }
        
        void pop_back ()
        {
            listNode* newTail;

            if (amountOfNodes > 0)
            {
                newTail = Tail->getPrevNode();
            }
            delete Tail;

            switch(amountOfNodes)
            {
                case 0:
                    std::cout << "There are no nodes already\n";
                    return;
                    break;

                case 1:
                    Tail = nullptr;
                    Head = nullptr;
                    break;

                case 2:
                    Tail = Head;
                    break;

                default:
                    Tail = newTail;
                    break;
            }

            --amountOfNodes;
        }

        void push_front (int newNodeValue)
        {
            if (amountOfNodes == 0)
            {
                listNode *newNode = new listNode(newNodeValue, nullptr, nullptr);
                Head = newNode;
                Tail = newNode;
                ++amountOfNodes;
                
                return;
            }
            if (amountOfNodes > 0)
            {
                listNode* newNode = new listNode(newNodeValue, nullptr, Head);
                Head = newNode;
                ++amountOfNodes;

                return;
            }
        }

        void pop_front ()
        {
            listNode* newHead;

            if (amountOfNodes > 0)
            {
                newHead = Head->getNextNode();
            }
            delete Head;

            switch(amountOfNodes)
            {
                case 0:
                    std::cout << "There are no nodes already\n";
                    return;
                    break;

                case 1:
                    Head = nullptr;
                    Tail = nullptr;
                    break;

                case 2:
                    Head = Tail;
                    break;

                default:
                    Head = newHead;
                    break;
            }

            --amountOfNodes;
        }

        ListIterator createIterator ()
        {
            if (amountOfNodes > 0)
            {
                ListIterator Iter(Head);
                return Iter;
            }
            else 
            {
                return 0;

                #ifdef DEBUG
                std::cout << "-IN_CREATE_ITER-\namountOfNodes == 0 !!!\n";
                #endif
            }
        }
        
        ListIterator createTailIterator ()
        {
            if (amountOfNodes > 0)
            {
                ListIterator Iter(Tail, amountOfNodes);
                return Iter;
            }
            else 
            {
                return 0;

                #ifdef DEBUG
                std::cout << "-IN_CREATE_ITER-\namountOfNodes == 0 !!!\n";
                #endif
            }
        }

        void viewListConsole ()
        {
            if (amountOfNodes > 0)
            {
                ListIterator Iter = createIterator();
                std::cout << "Node №" << Iter.getNodeNum() << ": " 
                    << Iter.getCurrentNode()->getValue() << std::endl;
                if (amountOfNodes > 1)
                {
                    for (int i = 2; i <= amountOfNodes; ++i)
                    {
                        Iter.moveForward();
                        std::cout << "Node №" << Iter.getNodeNum() << ": " 
                            << Iter.getCurrentNode()->getValue() << std::endl;
                    }
                }
            }
        }

        listNode* operator [ ] (int index)
        {
            if (index > int(amountOfNodes))
            {
                std::cout << "There's no [" << index << "] value in the list, as list length = " 
                    << amountOfNodes << "\n";
                return 0;
            }
            if (index < 0)
            {
                std::cout << "You gave a negative index: " << index << ". All indices must be positive\n";
                return 0;
            }
            if (index == 0)
            {
                std::cout << "List length: " << amountOfNodes << std::endl;
                return 0;
            }
            if (index <= (amountOfNodes - index))
            {
                ListIterator Iter = createIterator();
                while (Iter.getNodeNum() < index)
                {
                    Iter.moveForward();
                }
                return Iter.getCurrentNode();
            }
            else
            {
                ListIterator Iter = createTailIterator();
                while (Iter.getNodeNum() > index)
                {
                    Iter.moveBackwards();
                }
                return Iter.getCurrentNode();
            }

        }
        
        void swapValues (int index1, int index2)
        {
            if ((index1 != index2) && (index1 <= amountOfNodes) && (index2 <= amountOfNodes))
            {
                listNode* node1 = this->operator[](index1);
                listNode* node2 = this->operator[](index2);
                int swap = 0;

                swap = node1->getValue();
                node1->setValue(node2->getValue());
                node2->setValue(swap);
            }
            else 
            {
                std::cout << "Given indices (" << index1 << ", " << index2 << ") are not valid\n";
            }
        }

        #ifdef DEBUG
        void viewContents ()
        {
            std::cout << "-IN_VIEW_LIST_INTERFACE_CONTENTS-\n";
            std::cout << "First Node In List Ptr: " << Head << std::endl;
            if (amountOfNodes > 0) 
            {
                std::cout << "First Node In List Value: " << Head->getValue() << std::endl;
            }
            std::cout << "Last Node In List Ptr: " << Tail << std::endl;
            if (amountOfNodes > 0) 
            {
                std::cout << "Last Node In List Value: " << Tail->getValue() << std::endl;
            }
            else {std::cout << "No values in this List as amountOfNodes = 0\n";}
            std::cout << "Amount of Nodes: " << amountOfNodes << std::endl;
        }
        #endif
};


/* 
----- What to add -----
adding: 
    - to the middle
        - by value
        - by pointer
        - by [ ]
deleting:
    - to the middle
        - by value
        - by pointer
        - by number of node
Merger
Swap nodes 
    - by pointer
    - by value
Sort values
    - by cahging pointers
    - by changing only values
Reversal of the list
Make namespace

 */
/* 
----- What is ready -----
adding: 
    - to the beginning +
    - to the end +
deleting:
    - to the beginning +
    - to the end +
Swap nodes 
    - by [ ] +

Iterator +
Override [ ] +
 */
