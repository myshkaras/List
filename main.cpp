#include <iostream>
#include "classes.h"

using namespace std;

int main(int argc, char *argv[])
{
    ListInterface listInterface;

    listInterface.push_back(0);
    listInterface.push_back(1);
    listInterface.push_front(2);
    listInterface.push_back(3);
    listInterface.push_back(4);
    listInterface.push_front(5);
    //listInterface.viewContents();
    listInterface.viewListConsole();
    


    

    /* ListIterator Iter = listInterface.createIterator();
    Iter.viewContents();
    Iter.moveForward();
    Iter.viewContents();
    Iter.moveForward();
    Iter.viewContents();
    Iter.moveBackwards();
    Iter.viewContents(); */

    /* listInterface.pop_front();
    listInterface.viewInterfaceContents();
    listInterface.pop_front();
    listInterface.viewInterfaceContents();
    listInterface.pop_front();
    listInterface.viewInterfaceContents();
    listInterface.pop_front();
    listInterface.viewInterfaceContents(); */

    return 0;
}
